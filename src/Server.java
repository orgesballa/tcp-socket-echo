// A Java program for a Server

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.net.*;
import java.io.*;
import java.sql.SQLClientInfoException;
import java.util.Scanner;

public class Server {

    private Socket socket = null; // socket attribute
    private ServerSocket server = null; // ServerSocket attribute
    private DataInputStream in = null; // initialize the Input and Output Stream
    private DataOutputStream out = null;

    // constructor with port
    public Server(int port) {

        // Variables declaration the same as in the Client
        String line1 = "";
        String line2 = "";
        String line3 = "";

        String lines[];
        String lines2[];
        int SeqNum = 0;

        String pp = "";
        String pp2 = "";
        String measureType = "";
        int noProbes = 0;
        int messSize = 0;
        int serverDelay = 0;

        // starts server and waits for a connection
        try {
            server = new ServerSocket(port);     // Create the Socket
            System.out.println("Server started at port: " + port);

            System.out.println("Waiting for a client to be connected on this port");

            socket = server.accept(); // accept connection
            System.out.println("Client accepted");
            // takes input from the client socket
            in = new DataInputStream(
                    new BufferedInputStream(socket.getInputStream()));
            out = new DataOutputStream(socket.getOutputStream()); // this serves to echo the message to client

            // reads message from client
            try {
                line1 = in.readUTF(); // Read from client
                lines = line1.split("\\s+"); // Split in an array

                pp = lines[0]; // Protocol
                measureType = lines[1]; // Measurement Type
                noProbes = Integer.parseInt(lines[2]); // Number of Probes
                messSize = Integer.parseInt(lines[3]); // Message Size
                serverDelay = Integer.parseInt(lines[4]); // Server Delay
                System.out.println("Client has sent: " + line1);
                if (pp.equals("s") && (measureType.equals("rtt") || measureType.equals("tput")) && noProbes != 0
                        && messSize != 0) { // Condition to check if we have the right format. pp should be s
                    try {                   // measuretype can be either rtt or tput
                        // no Probes should be different from 0
                        // messSize should be different from 0
                        out.writeUTF("200 OK: Ready"); // If everything is alright, send 200 OK:READY
                    } catch (IOException i) {
                        System.out.println(i);
                    }
                } else {
                    try { // Error in the Connection Setup
                        out.writeUTF("404 ERROR: Invalid Connection Setup Message");  // inform the Client
                        socket.close();
                        in.close();
                        System.exit(0); // Terminate the program
                    } catch (IOException i) {
                        System.out.println(i);
                    }
                }
                /* If everything is alright, we go in the next step, otherwise the program must have ended */
                try {
                    for (int i = 1; i <= noProbes; i++) { // Receive as many messages as declared in noProbes
                        line2 = in.readUTF(); // read the message
                        lines2 = line2.split("\\s+"); // Split it in an array
                        pp2 = lines2[0]; // Protocol
                        SeqNum = Integer.parseInt(lines2[1]); // Sequence Number
                        System.out.println("Client sends: " + line2);
                        // PayLoad = lines2[1];

                        /* If sequence Number is not in the right order or protocol is not m, we terminate */
                        if (SeqNum != i || !pp2.equals("m")) {
                            System.out.println("404 ERROR: Invalid Measurement Message"); // Inform the server
                            out.writeUTF("404 ERROR: Invalid Measurement Message");
                            out.close();
                            in.close();
                            socket.close();
                            System.exit(0); // Terminate

                        }
                        if (serverDelay != 0) { // If serverDelay is not 0, then we put the Server program (Thread) to sleep,
                            try {               // so it waits then sends back the message
                                Thread.sleep(serverDelay * 1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        out.writeUTF(line2);
                    }
                } catch (IOException i) {
                    System.out.println(i);
                }
                line3 = in.readUTF(); // Read from Client
                System.out.println(line3);
                if (line3.equals("t")) { // If we get a "t", we proceed
                    System.out.println("Correct information.");
                    out.writeUTF("200 OK: Closing Connection"); // Everything is alright, inform the Client to terminate
                    System.out.println("Correct information. Sending 200 OK: Closing Connection to Client");
                    socket.close();
                    out.close();
                    in.close();
                } else {
                    System.out.println("404 ERROR: Invalid Connection Termination Message"); // Otherwise terminate
                    out.writeUTF("404 ERROR: Invalid Connection Termination Message"); // Inform the Client
                    socket.close();
                    out.close();
                    in.close();
                    System.exit(0);
                }
            } catch (IOException i) {
                System.out.println(i);
            }

        } catch (IOException i) {
            System.out.println(i);
        }
    }

    public static void main(String args[]) {
        System.out.println("Please enter the port number of the server");// get the port number from the user
        Scanner sc = new Scanner(System.in); // Using scanner we get the input from console
        Server server = new Server(sc.nextInt()); // Create a Server object with the given port
    }
}
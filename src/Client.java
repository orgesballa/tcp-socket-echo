// A Java program for a Client

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Client {
    // initialize socket and input output streams
    private Socket socket = null; // Initialize the socket
    private DataInputStream input = null; // This serves as a command line reader
    private DataOutputStream out = null; // This serves as an output to the socket
    private DataInputStream serverResponse = null; // this is to use for Echo Message

    // constructor to put ip address and port
    public Client(String address, int port) {
        // establish a connection
        try {
            socket = new Socket(address, port);
            System.out.println("Connected"); // Print Connected if connected

            // takes input from command line
            input = new DataInputStream(System.in);

            //takes input from Socket ( echo message)
            serverResponse = new DataInputStream(socket.getInputStream());

            // sends output to the socket
            out = new DataOutputStream(socket.getOutputStream());
        } catch (UnknownHostException u) {
            System.out.println(u); // In case of UnknownHost
        } catch (IOException i) {
            System.out.println(i);
        }

        String lines[];       // Used to split the user inputs
        String lines2[];      // Used for second phase
        String lines3[];      // Used for third phase

        String line = "";     // Store the user lines
        String line2 = "";    // Store the user lines in second phase
        String line3 = "";    // Store the user lines in third phase

        String response = "";  // this is to store the response from server in the first phase
        String response2 = ""; // this is to store the response from server in the second phase
        String response3 = ""; // this is to store the response from server in the third phase

        /*
        These variables are the user inputs
         */
        String MeasureType = "";
        int NoProbes = 0;
        int messSize = 0;
        int serverDelay = 0;
        int SeqNum = -1;
        String sentMsg = "";
        long startTime = 0;
        long endTime = 0;
        // string to read message from input
        double rtt;
        double tput;
        ArrayList<Double> list1 = new ArrayList<>();
        String pp = "";
        String pp2 = "";
        double Average = 0;
        double sum = 0;


        //Read message inside a try and catch block
        try {
            System.out.println("Enter the following information");
            System.out.println("The correct format is:<PROTOCOL PHASE><WS><MEASUREMENT TYPE><WS><NUMBER OF PROBES><WS><MESSAGE SIZE><WS><SERVER DELAY>");
            line = input.readLine(); // Console input from user
            lines = line.split("\\s+"); // Split the line by " " space and store in lines array

            pp = lines[0];  // Protocol
            MeasureType = lines[1]; // Measurement Type
            NoProbes = Integer.parseInt(lines[2]); // Number of Probes
            messSize = Integer.parseInt(lines[3]); // Message Size
            serverDelay = Integer.parseInt(lines[4]); // Server Delay
            out.writeUTF(line); // Send the line to server
            try {
                response = serverResponse.readUTF(); // Read the response from server
                if (response.equals("404 ERROR: Invalid Connection Setup Message")) { // If error, we terminate
                    System.out.println("404 ERROR: Invalid Connection Setup Message");
                    socket.close(); // close the socket
                    out.close();  // close in and out
                    input.close();
                    System.exit(0); // exit the program, don't execute what is after
                } else {
                    System.out.println("Response from server:" + response); //Print the message from server
                }
            } catch (IOException i) {
                System.out.println(i);
            }
            System.out.println("Enter the following information"); // After a successful setup, we proceed
            System.out.println("The correct format is:<PROTOCOL PHASE><WS><PROBE SEQUENCE NUMBER>");
            System.out.println("You should enter the information " + NoProbes + " times.");
            for (int i = 1; i <= NoProbes; i++) { // A loop to take NoProbes times messages
                line2 = input.readLine(); // Get the message from user
                lines2 = line2.split("\\s+"); // Split as in the above case
                pp2 = lines2[0]; // This is the protocol
                SeqNum = Integer.parseInt(lines2[1]); // Sequence Number
                StringBuilder buffer = new StringBuilder(messSize);
                for (int j=0;j<messSize;j++) {
                        buffer.append('a');// Since we want to send different sized messages, we create byte arrays
                }
                sentMsg = pp2 + " " + SeqNum + " " + buffer; // Build the string to send: Protocol + SeqNum + Payload
                startTime = System.nanoTime(); // Start the timer
                out.writeUTF(sentMsg); // Send the message
                response2 = serverResponse.readUTF(); // Get the response
                endTime = System.nanoTime(); // End the time
                System.out.println("Server echoes back: " + response2);
                if (response2.equals("404 ERROR: Invalid Measurement Message")) { // If error, we terminate
                    System.out.println("404 ERROR: Invalid Measurement Message");
                    System.out.println("Failure, closing connection");
                    socket.close();
                    out.close();
                    input.close();
                    System.exit(0);
                    break;
                } else if (!response2.equals(sentMsg)) { // If we get a different message from what we sent, we terminate
                    System.out.println("Error:Not the right sequence ");
                    System.out.println("Failure, closing connection");
                    socket.close();
                    out.close();
                    input.close();
                    System.exit(0);
                }

                rtt = (double) (endTime - startTime); // Calculate the RTT
                if (MeasureType.equals("rtt")) {   // If measurement is RTT , we add it in the list
                    list1.add(rtt);
                } else if (MeasureType.equals("tput")) { // If it is TPUT, we calculate TPUT as MessageSize / RTT and add it in the list
                    tput = (double) messSize / rtt;
                    tput = tput * 1000000000;
                    list1.add(tput);
                }
            }

            System.out.println("Enter the following information"); // NEXT PHASE
            System.out.println("<PROTOCOL PHASE><WS>");
            line3 = input.readLine(); // Read from user
            lines3 = line3.split(" "); // Same procedure
            out.writeUTF(lines3[0]); // Send to server
            response3 = serverResponse.readUTF(); // Get the response
            if (response3.equals("200 OK: Closing Connection")) { // If we get a 200 OK, we terminate
                System.out.println(response3);
                socket.close();
                out.close();
                input.close();
            } else if (response3.equals("404 ERROR: Invalid Connection Termination Message")) {
                System.out.println(response3);
                socket.close();               // If we get an error, we terminate again, but in this case we don't do calculate,
                out.close();                  // just a brute force termination
                input.close();
                System.exit(0);
        }

            // For each element in the list, we add it to the sum
            for (Double aList1 : list1) {
                System.out.println("Element: "+ aList1);
                sum += aList1;
            }
            Average = sum/list1.size(); //Find the average which is sum over size
            System.out.println("Average measurement for " +NoProbes + " messages with size "+ messSize + " is: " + Average);
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    public static void main(String args[]) {
        System.out.println("Please enter the host IP address and the port of the server"); // Tell user to enter the IP address and the port number
        Scanner sc = new Scanner(System.in); // Using scanner, we get it from console
        Client client = new Client(sc.next(), sc.nextInt()); // Create a client object with an IP and Port
    }
}